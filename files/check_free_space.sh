#!/bin/sh

if [ $# -gt 2 ]; then
    echo $0 checks all local block devices for usage, and sends a warning if the specified threshold is surpassed.
    echo
    echo Usage:    $0 [threshold in percent] [mount point]
    echo        eg $0 80 /mnt/backup
    exit 1
fi

SEND_MAIL=0
MESSAGE="`readlink -f $0` on `hostname`.`dnsdomainname`: The following devices are above $1% in capacity:\n"

if [ $1 ]; then
    WARN_PERCENT=$1
else
    WARN_PERCENT=80
fi

if [ $2 ]; then
    MOUNT_POINT=$2
else
    MOUNT_POINT=''
fi

if [ "$MOUNT_POINT" = "" ]; then
    for block_device in `ls /dev/disk/by-uuid/*`; do

    # tail -n1 takes just the last line
    # cut cuts a line into segments along the delimiter given with -d, and -f tells which fields should be displayed
    # sed s/%// replaces % with nothing

        CURRENT=`df --output=pcent,source $block_device | tail -n1`
        CURRENT_PERCENT=`echo $CURRENT | cut -d ' ' -f 1 | sed 's/%//'`

        if [ "$CURRENT_PERCENT" -gt "$WARN_PERCENT" ]; then
            SEND_MAIL=1
            MESSAGE="$MESSAGE\n$CURRENT\n"
        fi

    done
else
    CURRENT=`df --output=pcent,source $MOUNT_POINT | tail -n1`
    CURRENT_PERCENT=`echo $CURRENT | cut -d ' ' -f 1 | sed 's/%//'`

    if [ "$CURRENT_PERCENT" -gt "$WARN_PERCENT" ]; then
        SEND_MAIL=1
        MESSAGE="$MESSAGE\n$CURRENT\n"
    fi
fi


if [ "$SEND_MAIL" -eq 1 ]; then
    echo $MESSAGE
    exit 1
else
    exit 0
fi
